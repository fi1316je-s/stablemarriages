use std::iter::FromIterator;
use std::io::BufRead;
use std::io::{self, Read};
use elapsed::measure_time;
use std::thread::{self, JoinHandle};
use std::sync::Arc;

struct Man {
    id: usize,
    prefs: Vec<u16>,
    propose_to: usize,
}
struct Woman {
    id: usize,
    invprefs: Vec<u16>,
    engaged_to: usize,
}

fn parse_ints(v: Arc<Vec<u8>>, start: usize, stop: usize) -> Vec<u16> {
    let v = &v[start..stop];
    let len = v.len()/2+1;
    let mut numbers = Vec::with_capacity(len);
    unsafe { numbers.set_len(len); }
    let mut acc = 0;
    let mut n = 0;
    for b in v.iter() {
        if *b == 0x20 || *b == 0x0a {
            unsafe { *numbers.get_unchecked_mut(n) = acc; }
            acc = 0;
            n += 1;
            continue;
        }
        acc *= 10;
        acc += (*b-0x30) as u16;
    }
    unsafe { numbers.set_len(n); }
    numbers
}

// Faster single threaded performance
// fn read_ints() -> (usize, Vec<u16>) {
//     let stdin = io::stdin();
//     let mut handle = stdin.lock();    
//     let mut len = String::new();
//     handle.read_line(&mut len).expect("Couldn't read first line");
//     let n: usize = len[..len.len()-1].parse().expect("Couldn't parse first row to single u32");
//     let len = (n+1)*n*2;
//     let mut numbers: Vec<u16> = Vec::with_capacity(len);
//     unsafe { numbers.set_len(len); }
//     let mut buf: Vec<u8> = Vec::new();
//     handle.read_to_end(&mut buf).expect("Couldn't read to end");
    
//     let mut bytes = buf.iter();
//     for num in numbers.iter_mut() {
//         let mut b = bytes.next().expect("Couldn't get next byte 1");
//         let mut acc: u16 = 0;
//         while *b != 0x20 && *b != 0x0a {
//             acc *= 10;
//             acc += (*b-0x30) as u16;
//             b = bytes.next().expect("Couldn't get next byte 2");
//         }
//         *num = acc;
//     }

//     (n, numbers)
// }

fn read_ints_parallel() -> (usize, Vec<u16>) {
    let stdin = io::stdin();
    let mut handle = stdin.lock();    
    let mut len = String::new();
    handle.read_line(&mut len).expect("Couldn't read first line");
    let n: usize = len[..len.len()-1].parse().expect("Couldn't parse first row to single u32");
    let mut buf: Vec<u8> = Vec::new();
    handle.read_to_end(&mut buf).expect("Couldn't read to end");
    let buf: Arc<Vec<u8>> = Arc::new(buf);
    
    let n_threads: usize = 10;
    let mut handle_list: Vec<JoinHandle<Vec<u16>>> = Vec::with_capacity(n_threads);
    let len = buf.len();
    let chunk_size = len / n_threads;
    let mut last_stop = 0;
    for i in 1..n_threads+1 {
        let start = last_stop;
        last_stop = chunk_size * i;
        loop {
            match last_stop {
                i if i == len => break,
                i if buf[i] == 0x20 || buf[i] == 0x0a => { last_stop += 1; break; },
                _ => last_stop += 1,
            }
        }
        let buf = Arc::clone(&buf);
        let h = thread::spawn(move || {
            parse_ints(buf, start, last_stop)
        });
        handle_list.push(h);
    }

    let res_len = (n+1)*n*2;
    let mut numbers: Vec<u16> = Vec::with_capacity(res_len);
    for h in handle_list.into_iter() {
        let mut part = h.join().unwrap();
        numbers.append(&mut part);
    }
    (n, numbers)
}

fn inv_pref(v: &[u16]) -> Vec<u16> {
    let mut res: Vec<u16> = Vec::with_capacity(v.len());
    unsafe { res.set_len(v.len()); }
    for (i, num) in v.iter().enumerate() {
        //res[*num as usize - 1] = i as u16;
        unsafe { *res.get_unchecked_mut(*num as usize - 1) = i as u16; }
    }
    res
}

fn get_initial_person_lists(n: usize, preflist: Vec<u16>) -> (Vec<Woman>, Vec<Man>) {
    let mut men: Vec<Man> = Vec::with_capacity(n);
    let mut women: Vec<Woman> = Vec::with_capacity(n);
    men.resize_with(n, || Man{ id: 0, prefs: Vec::new(), propose_to: 0 } );
    women.resize_with(n, || Woman{ id: 0, invprefs: Vec::new(), engaged_to: 0 } );
    for prefs in preflist.chunks(n+1) {
        let new_id = prefs[0] as usize;
        let Woman{id,invprefs,engaged_to:_} = &mut women[new_id-1];
        if 0 == *id {
            *id = new_id;
            *invprefs = inv_pref(&prefs[1..]);
        } else {
            let Man{id,prefs:old_prefs,propose_to:_} = &mut men[new_id-1];
            *id = new_id;
            *old_prefs = prefs[1..].to_vec();
        }
    }
    (women, men)
}

fn stable_marriages() {
    let (n, preflist) = read_ints_parallel();
    let (mut women, mut men) = get_initial_person_lists(n, preflist);
    let mut unengagedmen: Vec<usize> = Vec::from_iter(1..n+1);
    while !unengagedmen.is_empty() {
        let unengagedmen_copy = unengagedmen.clone();
        unengagedmen.clear();
        for id in unengagedmen_copy.iter() {
            let Man{id,prefs,propose_to} = &mut men[*id-1];
            *propose_to += 1;
            let wpref = prefs[*propose_to-1] as usize;
            let Woman{id:_,invprefs, engaged_to} = &mut women[wpref-1];
            if *engaged_to == 0 || invprefs[*engaged_to-1] > invprefs[*id-1] {
                if *engaged_to != 0 {
                    unengagedmen.push(*engaged_to);
                }
                *engaged_to = *id;
            } else {
                unengagedmen.push(*id);
            }
        }
    }
    let res = women.iter().fold(String::new(),|s,w| s + &w.engaged_to.to_string() + "\n");
    println!("{}",res);
}

fn main() {
    let (elapsed,_) = measure_time(|| {
        stable_marriages()
    });
    println!("{}", elapsed);
}

// curl https://sh.rustup.rs -sSf | sh