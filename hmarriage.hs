import Data.List (elemIndex, sortOn)
import Data.Maybe (fromMaybe)
import Data.Array

data Man = Man { i :: Int
               , prefs :: Array Int Int
               , proposeTo :: Int
               } deriving (Show)

data Woman = Woman { invPrefs :: Array Int Int
                   , engagedTo :: Int
                   } deriving (Show)

main :: IO ()
main = do
    input <- getContents
    let (n:xs) = parseInts input
        (un, men, wom)  = makePersonLists [] [] $ sortOn head $ splitIntoChunks (n+1) xs
    putStrLn $ womenToString $ stableMarriages un [] men wom
          
parseInts :: String -> [Int]
parseInts s = concatMap (map read . words) $ lines s

invPerm :: [Int] -> [Int]
invPerm xs = [fromMaybe (error "Not invertable") $ elemIndex x xs| x <- [1..end]]
    where end = length xs
    
splitIntoChunks :: Int -> [Int] -> [[Int]]
splitIntoChunks _ [] = []
splitIntoChunks n xs = a:splitIntoChunks n b
    where (a, b) = splitAt n xs

makePersonLists :: [Man] -> [Woman] -> [[Int]] -> ([Int], Array Int Man, Array Int Woman)
makePersonLists _ _ ([]:_) = error "Can't make person lists"
makePersonLists men women [] = ([0..len], sortedArray men, sortedArray women)
    where len = length men - 1
          sortedArray = toArray . reverse
makePersonLists men women ((i:prefs):xs)
    | wlen == i = makePersonLists (Man{i=i-1,prefs=mPrefs,proposeTo=0}:men) women xs
    | otherwise = makePersonLists men (Woman{invPrefs=wInvPrefs,engagedTo=(-1)}:women) xs
    where wlen = length women
          mPrefs = toArray $ map pred prefs
          wInvPrefs = toArray $ invPerm prefs

toArray :: [a] -> Array Int a
toArray xs = listArray (0,len) xs
    where len = length xs - 1

stableMarriages :: [Int] -> [Int] -> Array Int Man -> Array Int Woman -> Array Int Woman
stableMarriages [] [] _ women  = women
stableMarriages [] newUnengMen men women  = stableMarriages newUnengMen [] men women
stableMarriages (i:unengMen) newUnengMen men women
    | engagedTo == -1 = stableMarriages unengMen newUnengMen newMen newWomen
    | wannaSwitch prefW i = stableMarriages unengMen (engagedTo:newUnengMen) newMen newWomen
    | otherwise = stableMarriages unengMen (i:newUnengMen) newMen women
    where (Man _ prefs proposeTo) = men ! i
          prefI = prefs ! proposeTo
          prefW@(Woman invPrefs engagedTo) = women ! prefI
          updatedMan = Man i prefs (proposeTo+1)
          newMen = men//[(i,updatedMan)]
          newWomen = women//[(prefI,Woman invPrefs i)]

womenToString :: Array Int Woman -> String
womenToString women = unlines $ map (\i -> show . succ . engagedTo $ women ! i) [0..len]
    where len = length women - 1

wannaSwitch :: Woman -> Int -> Bool
wannaSwitch (Woman invPrefs engagedTo) i = new < current
    where current = invPrefs ! engagedTo
          new = invPrefs ! i
